*** Settings ***
Library  SeleniumLibrary
Library  OperatingSystem
Library  String
Library  RequestsLibrary

#Resource  ${EXEC_DIR}/resources.robot
Test Setup  Run Keywords   Setup Configuration  Delete Existing Invoices
Test Teardown  Run Keywords    Close Browser


*** Test Cases ***
Add Invoice
    [Tags]    342341
    # The purpose of the test is to see if we can add an invoice. this
    # section is more about setup and test data management -- calling an
    # API to delete existing data, so that we have a clean testbed.
    # Let's "extract keyword" (like "extract method") to denote this as "Delete Existing Invoices"
    # and move into the test setup.

    Open Browser    http://34.225.240.91		chrome

    Click Link  Add Invoice

    Input Text  invoice   123
    Input Text  company   Beaufort Fairmont, LLC
    Input Text  type   Consulting for Test Automation
    Input Text  price   $10,000.00
    Select From List By Value   selectStatus    Past Due
    Input Text  dueDate   3/1/2019
    Input Text  comment   Assessing current testing and automation efforts and providing recommendations for moving forward with successful test automation.
    Click Button    createButton

    Page Should Contain     Beaufort Fairmont, LLC


*** Keywords ***
Setup Configuration
    # Requires Chromedriver in Path (See earlier Excercise)
    Set Environment Variable    PATH  %{PATH}:${EXECDIR}/../drivers
    Set Selenium Implicit Wait    1 Seconds
    Set Selenium Speed     .07 seconds

Delete Existing Invoices
    Create Session	invoiceAPISession     http://34.197.198.114:8081
    Delete Request  invoiceAPISession    /deleteAllInvoices
