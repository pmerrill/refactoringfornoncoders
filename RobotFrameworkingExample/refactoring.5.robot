*** Settings ***
Library  SeleniumLibrary
Library  OperatingSystem
Library  String
Library  RequestsLibrary

Resource  ${EXEC_DIR}/resources.robot
Test Setup  Run Keywords   Setup Configuration  Delete Existing Invoices
Test Teardown  Run Keywords    Close Browser


*** Test Cases ***
Add Invoice
    [Tags]    342341

    Open Browser    ${SiteUrl}		${Browser}

    # We had 9 lines below to create an invoce. Let's create a keyword called
    # "Create Invoice" and give it the data we want.
    Create Invoice    123    Beaufort Fairmont, LLC    Consulting for Test Automation    $10,000.00    3/1/2019    Assessing current testing and automation efforts and providing recommendations for moving forward with successful test automation.    Past Due

    Page Should Contain     Beaufort Fairmont, LLC


*** Keywords ***
Setup Configuration
    # Requires Chromedriver in Path (See earlier Excercise)
    Set Environment Variable    PATH  %{PATH}:${EXECDIR}/../drivers
    Set Selenium Implicit Wait    1 Seconds
    Set Selenium Speed     .07 seconds

Delete Existing Invoices
    Create Session	invoiceAPISession    ${ApiUrl}
    Delete Request  invoiceAPISession    /deleteAllInvoices

Create Invoice
    [Arguments]  ${Name}    ${Company}  ${Type}     ${Cost}     ${Date}     ${Comments}     ${Status}
    Click Link  Add Invoice

    Input Text  invoice   ${Name}
    Input Text  company   ${Company}
    Input Text  type   ${Type}
    Input Text  price   ${Cost}
    Select From List By Value   selectStatus    ${Status}
    Input Text  dueDate   ${Date}
    Input Text  comment   ${Comments}
    Click Button    createButton
