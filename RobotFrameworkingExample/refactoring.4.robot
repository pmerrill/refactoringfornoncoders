*** Settings ***
Library  SeleniumLibrary
Library  OperatingSystem
Library  String
Library  RequestsLibrary

# Pull in a resource file where we've moved common values, which are now
# variables.
Resource  ${EXEC_DIR}/resources.robot
Test Setup  Run Keywords   Setup Configuration  Delete Existing Invoices
Test Teardown  Run Keywords    Close Browser


*** Test Cases ***
Add Invoice
    [Tags]    342341

    # Hardcoded strings (like this url) are difficult to change and have little
    # meaning. Let's "introduce variable" and "pull up to resource file" (like
    # "pull up to superclass") to reduce maintenance later, and create meaning
    # now. Let's also do the same with the browser driver. Also do this for the APIUrl further down in the keywords.
    Open Browser    ${SiteUrl}		${Browser}

    Click Link  Add Invoice

    Input Text  invoice   123
    Input Text  company   Beaufort Fairmont, LLC
    Input Text  type   Consulting for Test Automation
    Input Text  price   $10,000.00
    Select From List By Value   selectStatus    Past Due
    Input Text  dueDate   3/1/2019
    Input Text  comment   Assessing current testing and automation efforts and providing recommendations for moving forward with successful test automation.
    Click Button    createButton

    Page Should Contain     Beaufort Fairmont, LLC


*** Keywords ***
Setup Configuration
    # Requires Chromedriver in Path (See earlier Excercise)
    Set Environment Variable    PATH  %{PATH}:${EXECDIR}/../drivers
    Set Selenium Implicit Wait    1 Seconds
    Set Selenium Speed     .07 seconds

Delete Existing Invoices
    Create Session	invoiceAPISession     ${ApiUrl}
    Delete Request  invoiceAPISession    /deleteAllInvoices
